import json
from urllib.parse import urlparse, quote
import urllib.request
from datetime import datetime
import sys
import time
import subprocess
from functools import wraps
from statistics import mean


def retry(ExceptionToCheck, tries=20, delay=1, backoff=1.1, logger=None):
    """Retry calling the decorated function using an exponential backoff.

    http://www.saltycrane.com/blog/2009/11/trying-out-retry-decorator-python/
    original from: http://wiki.python.org/moin/PythonDecoratorLibrary#Retry

    :param ExceptionToCheck: the exception to check. may be a tuple of
        exceptions to check
    :type ExceptionToCheck: Exception or tuple
    :param tries: number of times to try (not retry) before giving up
    :type tries: int
    :param delay: initial delay between retries in seconds
    :type delay: int
    :param backoff: backoff multiplier e.g. value of 2 will double the delay
        each retry
    :type backoff: int
    :param logger: logger to use. If None, print
    :type logger: logging.Logger instance
    """
    def deco_retry(f):
        @wraps(f)
        def f_retry(*args, **kwargs):
            mtries, mdelay = tries, delay
            while mtries > 1:
                try:
                    return f(*args, **kwargs)
                except ExceptionToCheck as e:
                    msg = "%s, Retrying in %d seconds..." % (str(e), mdelay)
                    if logger:
                        logger.warning(msg)
                    else:
                        print(msg)
                    time.sleep(mdelay)
                    mtries -= 1
                    mdelay *= backoff
            return f(*args, **kwargs)

        return f_retry  # true decorator

    return deco_retry

def heishamon_command(command, value):
     subprocess.run(f'mosquitto_pub -t panasonic_heat_pump/commands/{command} -m {value} -u heishamon -P heishamon'.split(' '))

def heishamon_SetForceDHW():
    heishamon_command('SetForceDHW', '1')

@retry(Exception)
def heishamon_EnableHeat():
     with urllib.request.urlopen('http://heishamon.local/command?SetOperationMode=4') as response:
            print(response.url, response.read())

@retry(Exception)
def heishamon_DisableHeat():
     with urllib.request.urlopen('http://heishamon.local/command?SetOperationMode=3') as response:
            print(response.url, response.read())

@retry(Exception)
def heishamon_SetHeatRequestTemperature(new_shift):
    print('-> set new water shift', new_shift)
    with urllib.request.urlopen(f'http://heishamon.local/command?SetZ1HeatRequestTemperature={round(new_shift)}') as response:
        print(response.url, response.read())

@retry(Exception)
def heishamon_SetDemandControl(value):
    print('-> set demand control', value)
    with urllib.request.urlopen(f'http://heishamon.local/command?SetDemandControl={value}') as response:
        print(response.url, response.read())

def influxquery(query):
    req = urllib.request.Request(
        url="https://influxdb.kanthaus.online:443/query?db=kanthaus&q=" + quote(query) + "&epoch=ms",
        headers={
            'Authorization': 'Basic a2FudGhhdXMtcmVhZGVyOlh6elZxa3lsUGZVMUMyT294elRU'
        }
    )
    u = urllib.request.urlopen(req)
    return json.load(u)

def weather():
    return json.load(urllib.request.urlopen('https://dwd.api.proxy.bund.dev/v30/stationOverviewExtended?stationIds=P0292'))['P0292']

def interpolate(x, x0, x1, y0, y1):
    lower = min(y0, y1)
    upper = max(y0, y1)
    return min(upper, max(lower, y0 + (x - x0)*(y1 - y0)/(x1 - x0)))

print('\n---------------')
print(datetime.now())

room_temp = influxquery("""
    SELECT mean(temperature) / 100 
    FROM "homeautomation.Environment" 
    WHERE time >= now() - 5m AND (node_id = '15' OR node_id = '21' OR node_id = '19')
""")["results"][0]["series"][0]["values"][0][1]

previous_room_temp = influxquery("""
    SELECT mean(temperature) / 100 
    FROM "homeautomation.Environment" 
    WHERE time >= now() - 35m AND time < now() - 30m AND (node_id = '15' OR node_id = '21' OR node_id = '19')
""")["results"][0]["series"][0]["values"][0][1]

outdoor_temp = influxquery("""
    SELECT mean(outdoor_temp) 
    FROM "housebus.heating.heating_status.1.0" 
    WHERE time >= now() - 30m
""")["results"][0]["series"][0]["values"][0][1]

outdoor_temp_24h = influxquery("""
    SELECT mean(outdoor_temp) 
    FROM "housebus.heating.heating_status.1.0" 
    WHERE time >= now() - 24h
""")["results"][0]["series"][0]["values"][0][1]

shift = influxquery("""
    SELECT last(heat_shift_target_temp) 
    FROM "housebus.heating.heating_status.1.0"
""")["results"][0]["series"][0]["values"][0][1]

flow_temp = influxquery("""
    SELECT last(flow_temp) 
    FROM "housebus.heating.heating_status.1.0"
""")["results"][0]["series"][0]["values"][0][1]

flow_target_temp = influxquery("""
    SELECT last(flow_target_temp) 
    FROM "housebus.heating.heating_status.1.0"
""")["results"][0]["series"][0]["values"][0][1]

hot_water_mode = influxquery("""
    SELECT max(valve_position_dhw) 
    FROM "housebus.heating.heating_status.1.0"
    WHERE time >= now() - 29m
""")["results"][0]["series"][0]["values"][0][1]

defrost = influxquery("""
   SELECT max(defrost_running) 
    FROM "housebus.heating.heating_status.1.0"
    WHERE time >= now() - 20m
""")["results"][0]["series"][0]["values"][0][1]

#electric_export = influxquery("""
#   SELECT mean(export) - mean(import)
#   FROM (
#        SELECT derivative(value, 1h) / 10 AS export
#        FROM "homeautomation.Obis"
#        WHERE (code = '2.8.0' and node_id = '4') AND time > now() - 10m
#    ),(
#        SELECT derivative(value, 1h) / 10 AS import
#        FROM "homeautomation.Obis"
#        WHERE (code = '1.8.0' and node_id = '4') AND time > now() - 10m
#    )
#
#""")["results"][0]["series"][0]["values"][0][1]
ee_vals = [val for time, val in influxquery("""
    SELECT mean("value") FROM "homeautomation.Obis"
    WHERE (code = '16.7.0' and node_id = '4') AND time > now() - 10m
    GROUP BY time(1m)
""")["results"][0]["series"][0]["values"]]
electric_export = -mean(ee_vals + ee_vals[6:] + ee_vals[9:] + ee_vals[10:])

solar_forecast = [val for time, val in influxquery("""
    SELECT kw * 1000
    FROM "solarforecast"
    WHERE time >= now() and time < now() + 5h
""")["results"][0]["series"][0]["values"]]

dhw_temp = influxquery("""
   SELECT last(dhw_actual_temp) 
    FROM "housebus.heating.heating_status.1.0"
""")["results"][0]["series"][0]["values"][0][1]

compressor_freq = influxquery("""
   SELECT mean(compressor_frequency) 
    FROM "housebus.heating.heating_status.1.0"
    WHERE time >= now() - 1m
""")["results"][0]["series"][0]["values"][0][1]

print('previous', round(previous_room_temp, 2), 'room temp', round(room_temp, 2), 'shift', shift, 'hot water mode', hot_water_mode, 'defrost', defrost, 'export', electric_export, 'outdoor temp', round(outdoor_temp, 2), 'compressor_freq', round(compressor_freq, 2), 'flow_temp', flow_temp, 'flow_target_temp', flow_target_temp)

if shift < -5 or shift > 5:
    raise Exception('wrong shift, maybe heatpump is not running in heatshift mode?', shift)

if hot_water_mode > 0 and compressor_freq >= 19:
    print('hot water mode was active. wait until system has heated up. quitting...')
    sys.exit()

room_temperature_target = interpolate(outdoor_temp_24h, -5, 12, 19, 20.5)
hour = datetime.now().hour
if hour >= 20 or hour <= 6:
    print('night reduction active')
    room_temperature_target = 19 
print('room_temperature_target', round(room_temperature_target, 4))

# is DHW cold?
# indoor temperatures mostly there?
# sun shining or outdoors not getting warmer in the next hours?
# ...but if it's warm outside, we can wait for both conditions to be ideal
if dhw_temp < 48 and room_temp > room_temperature_target:
    if electric_export > 1000 and all(x > 2000 for x in solar_forecast[:2]) and (outdoor_temp_24h < 6 or room_temp > room_temperature_target): 
        print('sun is shining, trigger dhw!')
        heishamon_SetDemandControl(80)
        heishamon_SetForceDHW()
        sys.exit()

    if outdoor_temp_24h < 6:
        # get temperature forecast, filter out past data ('32767' because Wurzen is a forecast-only station)
        forecast_hours = round(interpolate(dhw_temp, 47, 40, 18, 4))
        outdoor_temp_forecast = [x/10 for x in weather()['forecast1']['temperature'] if x < 32767][:forecast_hours]
        print(outdoor_temp_forecast) 
        if all(x < outdoor_temp_forecast[0] for x in outdoor_temp_forecast[2:]):
            print('getting colder outside, trigger dhw!')
            heishamon_SetDemandControl(255)
            heishamon_SetForceDHW()
            sys.exit()

if defrost > 0:
    print('defrost was active. quitting...')
    sys.exit()

if compressor_freq < 1: 
    if room_temp < room_temperature_target - 2:
        print('rooms really cold, try to enable heating...')
        heishamon_SetDemandControl(255)
        heishamon_EnableHeat()
    elif electric_export > interpolate(room_temperature_target - room_temp, 0, 2, 2000, 0) and room_temp < room_temperature_target:
        # the colder it is in the house, the lower the solar export threshold will be until we turn on heating
        print('rooms not warm enough and we have solar power, try to enable heating...')
        heishamon_SetDemandControl(80)
        heishamon_EnableHeat()
        heishamon_SetHeatRequestTemperature(-5)
    sys.exit()

room_temp_diff = room_temp - previous_room_temp
overheating_offset = interpolate(outdoor_temp_24h, -5, 12, 1.2, 0.2)

new_shift = None
if electric_export > 500:
    if room_temp < room_temperature_target:
        new_shift = shift + round(interpolate(electric_export, 500, 4000, 1, 10))
        print('sun is shining! push up the temperature...')
    elif room_temp > room_temperature_target + overheating_offset + 1:
        if compressor_freq < 20 and flow_temp > flow_target_temp:
            if room_temp > room_temperature_target + overheating_offset + 2:
                heishamon_DisableHeat()
            sys.exit()
        new_shift = shift - 2 + (flow_temp - flow_target_temp)
elif room_temp > room_temperature_target + overheating_offset and electric_export < -100 and all(x < 1300 for x in solar_forecast):
    print('rooms really warm and no solar power, time for a heating pause')
    heishamon_DisableHeat()
elif room_temp > room_temperature_target + 0.2:
    if compressor_freq < 20 and flow_temp > flow_target_temp:
        print('flow temp above flow target, compressor frequency low. risk of shutdown, do not reduce water target')
        if room_temp > room_temperature_target + overheating_offset:
            print('rooms really warm. time for a heating pause...')
            heishamon_DisableHeat()
        sys.exit()
    if room_temp < room_temperature_target + 0.4 and room_temp_diff < -0.2:
        print('room cooling down fast, do not reduce target further. quitting...')
        sys.exit()

    new_shift = shift - 2 + (flow_temp - flow_target_temp)
elif room_temp_diff > 0.13:
    if room_temp > room_temperature_target - 0.1:
        print('room target reached, but still rising')
        if flow_temp < flow_target_temp:
            new_shift = shift - 2
        else:
            new_shift = shift - 1
    else:
        print('room is still heating up fast, no need for more power yet. quitting...')
        sys.exit()
elif room_temp < room_temperature_target - 0.3:
    new_shift = shift + 1

# prevent inefficient de-icing by keeping water warm enough
min_shift = -5 if outdoor_temp > 1 else -2

if new_shift:
    new_shift = max(min_shift, min(5, new_shift))

if new_shift is not None and new_shift != shift:
    if new_shift > shift and flow_temp <= flow_target_temp - 2:
        print('flow temp below target. wait until system has heated up before changing target. quitting...')
        sys.exit()
    heishamon_SetHeatRequestTemperature(new_shift)
